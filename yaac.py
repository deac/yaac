#! /usr/bin/python
import DataGenerator
class MakefileWriter:
    """This class takes various high level instructions and generates a makefile.

It can:
  - Exclude certain sources from certain targets.
  - Combine compile flag lists

The members are;
  - sharedSources:
      list of str. Filenames of C++ files that are built into every target.
  - directories:
      list of str. All of the directories that are used need to be in here. I plan to auto-generate this later.
  - targets:
      list of str. Might change it later so it could also be just a str. Every build target.
  - combiningTargets
      list of list of str. 
  - compileFlags:
      a str. A list of extra compile flags.
  - linkFlags:
      a str. A list of extra link flags.
  - uniqueSources:
      dict of list of str or just dict of str. Sources that are only built for their mapped string (used as a regex)
    """
    def __init__(self, generator, targets = ["a.out"], compileFlags = "-g -std=c++0x -I .", linkFlags = "-std=c++0x -lSDL -lGL -lGLU -lCEGUIBase -lCEGUIOpenGLRenderer -ltinyxml -lGLEW", uniqueSources = None, extraDependencies = None):
        """
        The constructor has default arguments that build a main.cpp to .bin/a.out.
        """
        self.generator = generator
        self.sharedSources = generator.getSources("a.out") #FIXME
        self.directories = generator.getDirectories()
        self.targets = targets
        self.compileFlags = compileFlags
        self.linkFlags = linkFlags
        self.uniqueSources = uniqueSources
        self.extraDependencies = extraDependencies
        if self.uniqueSources == None:
            self.uniqueSources = {}
            for target in targets:
                self.uniqueSources[target] = []
        self.actualTargets = ['.bin/' + i for i in targets]
        self.implicitDirectories = ['.obj', '.dep', '.bin']
        self.makefileName = "Makefile"

    def _getObjectList(self, sourceList):
        """Plumbing."""
        ret = []
        for i in sourceList:
            if i[-4:] == '.pcp':
                if i[-8:] == '.cpp.pcp':
                    item = '.obj/' + i[:-8] + '.o'
                    if item not in ret:
                        ret.append(item)
            else:
                ret.append('.obj/' + i[:-4] + '.o')
        return ret
        #return ['.obj/' + i[:-4] + '.o' for i in sourceList]

    def _finaliseDatastructures(self):
        """Plumbing."""
        self.sharedObjects = self._getObjectList(self.sharedSources)
        self.uniqueObjects = {}
        for target, sources in self.uniqueSources.items():
            self.uniqueObjects[target] = self._getObjectList(sources)
            
    def outputMakefile(self, extraRules=""):
        """Generates a makefile."""
        self._finaliseDatastructures()        
        self._outputMakefile(extraRules)

    def _outputMakefile(self, extraRules):
        """Plumbing."""
        file = open(self.makefileName, 'w')
        file.write('SHARED_OBJECTS=')
        for object in self.sharedObjects:
            file.write(' ' + object)
        file.write('\nTARGETS= ')
        for target in self.actualTargets:
            file.write(target + ' ')
        file.write('\nDIRECTORIES= ')
        for dir in self.directories:
            file.write('.dep/' + dir + ' .obj/' + dir + ' ')
        file.write('\n')
        for target in self.targets:
            file.write(target + '_OBJECTS= ')
            for object in self.uniqueObjects[target]:
                file.write(object + ' ')
        file.write('\nALL_TARGETS_COMBINED_DEPENDENCIES= ')
        file.write('$(ALL_SHARED_DEPENDENCIES)')
        for target in self.targets:
            file.write(' $(' + target + '_OBJECTS)')
        file.write('\nALL_SHARED_DEPENDENCIES= .dep .obj .bin $(DIRECTORIES) $(SHARED_OBJECTS)\n')
        file.write('DEPENDENCY_FILES= ')
        sourceSet = set()
        for sourceList in [self.sharedSources] + [sources for target, sources in self.uniqueSources.items()]:
            for source in sourceList:
                if source not in sourceSet:
                    sourceSet.add(source)
                    file.write(' .dep/' + source)
        file.write('\n\nall: $(ALL_TARGETS_COMBINED_DEPENDENCIES) $(TARGETS)\n')
        for target in self.targets:
            file.write('\n.bin/' + target + ': $(ALL_SHARED_DEPENDENCIES) $(' + target + '_OBJECTS)\n')
            file.write('\t@echo "Linking $@"\n')
            file.write('\t@g++ -o .bin/' + target + ' $(SHARED_OBJECTS) $(' + target + '_OBJECTS) ' + self.linkFlags + '\n')
        file.write('\n.obj/%.o: %.cpp\n')
        file.write('\t@echo "Compiling $@"\n')
        file.write('\t@g++ -MM $*.cpp ' + self.compileFlags + ' -MT \'.obj/$*.o\' -MF .dep/$*.cpp\n')
        file.write('\t@g++ -c $*.cpp -o $@ ' + self.compileFlags + '\n\n')
        file.write('%.tab.cpp: %.y\n')
        file.write('\tbison -d -o $*.tab.cpp $*.y\n\n')
        file.write('%.yy.cpp: %.l\n')
        file.write('\tflex -o$*.yy.cpp $*.l\n\n')
        file.write('\n-include $(DEPENDENCY_FILES)\n')
        file.write('.dep/%.pcp: %.pcp\n')
        file.write('\t@echo "Generating .dep/$*.pcp"\n')
        file.write('clean:\n')
        file.write('\t@echo "Removing all build targets, only generated file left is Makefile"\n')
        file.write('\t@rm -r -f ')
        for dir in self.implicitDirectories:
            file.write(dir + ' ')
        file.write('\n%.hpp:\n')
        file.write('\t@echo "File $*.hpp no longer exists"\n')
        file.write('%.h:\n')
        file.write('\t@echo "File $*.h no longer exists"\n')
        for dir in self.implicitDirectories:
            file.write(dir + ':\n\t@mkdir ' + dir + '\n')
        for dir in self.directories:
            file.write('.dep/' + dir + ':\n\t@mkdir .dep/' + dir + '\n')
            file.write('.obj/' + dir + ':\n\t@mkdir .obj/' + dir + '\n')
        file.write(extraRules)
            
#### ---- Unused data construction ----
#sharedSources = [
#] # CPP sources
#directories = [
#]
#targets = ['client', 'server']
#compileFlags = '-std=c++0x -I . -g'
#linkFlags = '-std=c++0x -g'

#uniqueSources = {}
#uniqueSources['client'] = ['client_main.cpp',
#]
#uniqueSources['server'] = ['server_main.cpp',
#]

import DirectorySearch
if __name__ == '__main__':
    gen = DataGenerator.DataGenerator()
    gen.excludedSources["a.out"] = ["./server_main.cpp"]
    gen.build()
    writer = MakefileWriter(gen)
    writer.outputMakefile()
