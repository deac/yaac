import os
import re
def searchByRegex(regexStr, ignoredRegexStrs = []):
    """This function searchs through the whole directory listing, returning all files that match regex, and don't match anything in ignoredRegexes
    """
    ret = []
    directoriesSet = set()
    regex = re.compile(regexStr)
    ignoredRegexes = [re.compile(str) for str in ignoredRegexStrs]
    for dir in os.walk('.'):
        for file in dir[2]:
            if regex.match(file) != None:
                matched = False
                for ignoredRegex in ignoredRegexes:
                    if ignoredRegex.search(dir[0] + '/' + file) != None:
                        matched = True
                        break
                if not matched:
                    ret.append(dir[0] + '/' + file)
                    if dir[0] not in directoriesSet:
                        parent = dir[0]
                        while True:
                            position = parent.rfind('/')
                            if position == -1:
                                break
                            else:
                                parent = parent[:position]
                            if parent not in directoriesSet:
                                directoriesSet.add(parent)
                        directoriesSet.add(dir[0])
    directories = [x for x in directoriesSet]
    directories.sort() # Might not be necessary
    print (directories)
    return [ret, directories]
