import re
import DirectorySearch
class DataGenerator:
    """This class should have generators for all data needed by MakefileWriter

    targets: a list of targets
        for target in dataGen.targets
    sources: a dict of list of sources, per target
        for source in dataGen.sources[$targetName]
    unitTests: a 
    """

    def __init__(self, _fileRegexList, _ignoreRegexesList):
        self.fileRegexList = _fileRegexList#'.*\.cpp'
        self.ignoreRegexesList = _ignoreRegexesList#["\\./\\..*", '.*\.cpp\.pcp']
        self.targets = []
        self.excludedSources = dict()
        self.additionalSources = dict()

    def build(self):
        sharedSourcesSet = []
        directoriesSet = []
        for i in range(len(self.fileRegexList)):
            fileRegex = self.fileRegexList[i]
            ignoreRegexes = self.ignoreRegexesList[i]
            #for fileRegex, ignoreRegexes in (self.fileRegexList, self.ignoreRegexesList):
            sharedSources = DirectorySearch.searchByRegex(fileRegex, ignoreRegexes)
            sharedSourcesSet += sharedSources[0]
            directoriesSet += sharedSources[1]
        self.sharedSources = sharedSourcesSet
        self.directories = directoriesSet
        #self.sharedSources = []
        #self.directories = []
        #for source in sharedSourcesSet:
            #if source not in self.sharedSources:
                #self.sharedSources.append(source)
        
            #FIXME make it run a uniqueness check on directories
    def getTargets(self):
        return self.targets
    def getSources(self, target):
        ret = self.sharedSources
        if target in self.excludedSources:
            for regexStr in self.excludedSources[target]:
                regex = re.compile(regexStr)
                ret = [source for source in ret if regex.match(source) == None]
        if target in self.additionalSources:
           ret = ret + self.additionalSources[target]
        return ret
    def getDirectories(self):
        dirs = []
        for dirName in self.directories:
            if dirName not in dirs:
                dirs.append(dirName)
        return dirs
    
